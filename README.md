Raspberry Pi Compute Module 4 2020 Bracket
==========================================

This is a simple bracket to attach a Raspberry Pi Compute Module 4 to a
piece of 2020 extrusion in your 3D printer.  The board is intended to be
attached face-down so that a Waveshare CM4-NANO-A (or something similar) can
be attached, with a 4-pin header on GPIO pins 4/6/8/10 added to connect to
a matching UART-and-power header on many 32-bit printer motherboards.

This particular arrangement of parts is set up to hold the boards on the
left side of my AM8, above the motherboard.  You could also mash the two
pieces together in FreeCAD (or maybe even in your slicer) if you want the
board held at a right angle relative to the frame mount.

Assembly requires four M2.5x5 (or thereabouts) screws to hold the CM4 to the
bracket, two M5x8 screws and hammer nuts to hold the spacer block to the
extrusion, and three M3x8 screws to attach the two pieces together.
